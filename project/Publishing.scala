// Copyright 2012 Atlassian PTY LTD
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import sbt._
import Keys._

object Publishing extends Plugin {
  val artifactory = "https://packages.atlassian.com/maven/"
  lazy val release = Some("releases" at artifactory + "private")
  lazy val snapshots = Some("snapshots" at artifactory + "private-snapshot")
  lazy val local = Some(Resolver.mavenLocal)
  lazy val localM2 = Some(Resolver.file("localm2", Path.userHome / ".m2" / "repository"))

  override def settings =
    Seq(
      publishTo <<= version { (v: String) =>
        if (v.trim endsWith "SNAPSHOT")
          localM2
        else
          release
      },
      publishMavenStyle := true,
      publishArtifact in Test := false,
      pomExtra :=
        <licenses>
          <license>
            <name>Apache License, Version 2.0</name>
            <url>https://www.apache.org/licenses/LICENSE-2.0</url>
            <distribution>repo</distribution>
          </license>
        </licenses>
        <scm>
          <url>https://bitbucket.org/atlassian/crowd-scala-rest-client</url>
          <connection>scm:git@bitbucket.org:atlassian/crowd-scala-rest-client.git</connection>
          <developerConnection>scm:git@bitbucket.org:atlassian/crowd-scala-rest-client.git</developerConnection>
        </scm>
      ,
      pomIncludeRepository := { (repo: MavenRepository) => false } // no repositories in the pom
    )
}
