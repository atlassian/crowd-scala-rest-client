import sbt._, Keys._
import com.typesafe.sbt.SbtScalariform
import scalariform.formatter.preferences._

object Scalariform {

  val customSettings = SbtScalariform.scalariformSettings ++
    Seq[Def.Setting[_]](
      SbtScalariform.ScalariformKeys.preferences := SbtScalariform.ScalariformKeys.preferences.value
          .setPreference(AlignParameters, true)
          .setPreference(RewriteArrowSymbols, true)
    )

}
