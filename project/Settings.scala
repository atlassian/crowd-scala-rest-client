import sbt._, Keys._
import sbtrelease._

object Settings {

  val testSettings = (testOptions in Test += Tests.Argument("console")) ++
    Seq(testListeners += new JUnitXmlTestsListener((target.value / ("scala-" + scalaBinaryVersion.value)).getAbsolutePath))

  lazy val standardSettings =
    Defaults.coreDefaultSettings ++
      testSettings ++
      Release.customReleaseSettings ++ // sbt-release
      Scalariform.customSettings ++ // sbt-scalariform
      Seq[Def.Setting[_]](
        organization := "com.atlassian.crowd.client",
        scalaVersion := "2.12.4",
        crossScalaVersions := Seq("2.11.8", "2.12.4"),
        autoScalaLibrary := false,
        unmanagedSourceDirectories in Compile += (sourceDirectory in Compile).value / ("scala_" + scalaBinaryVersion.value),
        scalacOptions := Seq(
          "-target:jvm-1.8",
          "-deprecation",
          "-unchecked",
          "-feature",
          "-Xlog-free-terms",
          "-Xlint",
          "-Ywarn-adapted-args",
          "-Ywarn-numeric-widen",
          "-Ywarn-value-discard",
          "-language:higherKinds",
          "-language:postfixOps",
          "-Xfatal-warnings",
          "-Ywarn-dead-code"
        ),
        resolvers ++= Seq(
          Resolver.defaultLocal,
          Resolver.mavenLocal,
          "atlassian-public" at "https://maven.atlassian.com/content/groups/atlassian-public/",
          "Tools Releases" at "http://oss.sonatype.org/content/repositories/releases",
          "Tools Snapshots" at "http://oss.sonatype.org/content/repositories/snapshots",
          "Scalaz Bintray Repo" at "http://dl.bintray.com/scalaz/releases" // For old scalaz-stream
        ),
        credentials += Credentials(Path.userHome / ".ivy2" / ".credentials"),
        mappings in(Compile, packageBin) ++= Seq(
          file("LICENSE") -> "META-INF/LICENSE",
          file("NOTICE") -> "META-INF/NOTICE"
        )
      )

}
