import sbt._
import sbt.Keys._
import Settings._

trait Modules {

  lazy val model =
    Project(
      id = "model",
      base = file("model"),
      settings = standardSettings
    )

  lazy val client =
    Project(
      id = "client",
      base = file("client"),
      settings = standardSettings
    ) dependsOn (
      model
    )

  lazy val all = 
    Project(
      id = "crowd-rest-client-parent",
      base = file("."),
      settings = standardSettings
    ) aggregate (
      model, client
    )
}
