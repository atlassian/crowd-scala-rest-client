object Versions {

  val akka = "2.4.19"
  val hamcrest = "2.0.0.0"
  val junit = "4.12"
  val mockito = "1.10.19"
  val monocle = "1.4.0"
  val scalaz = "7.2.12"
  val specs2 = "3.8.6"
  val akkaHttp = "10.0.10"
  val sprayJson = "1.3.6"

}
