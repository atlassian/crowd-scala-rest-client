name := "crowd-scala-rest-client"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor"             % Versions.akka,
  "com.typesafe.akka" %% "akka-slf4j"             % Versions.akka,
  "com.typesafe.akka" %% "akka-testkit"           % Versions.akka        % Test,
  "com.typesafe.akka" %% "akka-http-core"         % Versions.akkaHttp,
  "com.typesafe.akka" %% "akka-http"              % Versions.akkaHttp,
  "com.typesafe.akka" %% "akka-http-spray-json"   % Versions.akkaHttp,
  "io.spray"          %% "spray-json"             % Versions.sprayJson,
  "junit"             %  "junit"                  % Versions.junit       % Test exclude("org.hamcrest", "hamcrest-core"),
  "org.hamcrest"      %  "hamcrest-junit"         % Versions.hamcrest    % Test,
  "org.mockito"       % "mockito-core"            % Versions.mockito     % Test exclude("org.hamcrest", "hamcrest-core"),
  "org.scala-lang"    % "scala-library"           % scalaVersion.value,
  "org.specs2"        %% "specs2-core"            % Versions.specs2      % Test,
  "org.specs2"        %% "specs2-scalacheck"      % Versions.specs2      % Test,
  "org.specs2"        %% "specs2-junit"           % Versions.specs2      % Test,
  "org.specs2"        %% "specs2-mock"            % Versions.specs2      % Test
)

scalariformSettings
