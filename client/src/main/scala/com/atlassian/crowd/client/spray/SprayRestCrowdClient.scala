package com.atlassian.crowd.client.spray

import com.atlassian.crowd.client.async._
import com.atlassian.crowd.client.model._
import akka.actor.ActorSystem
import akka.util.Timeout

import scala.concurrent.{ ExecutionContext, Future }
import scala.concurrent.duration._
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers._
import akka.http.scaladsl.unmarshalling.{ FromResponseUnmarshaller, Unmarshal, Unmarshaller }
import akka.http.scaladsl.client.RequestBuilding._
import akka.stream.Materializer
import spray.json._
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._

import scalaz._
import Scalaz._

trait CrowdResponseTransformers {
  import CrowdJsonProtocol.errorEntityFormat

  implicit val errorEntityUnmarshaller: Unmarshaller[HttpEntity, ErrorEntity] =
    Unmarshaller
      .stringUnmarshaller
      .forContentTypes(ContentTypes.`application/json`)
      .map(_.parseJson.convertTo[ErrorEntity])

  def validateCrowdService(response: HttpResponse): Either[CrowdError, HttpResponse] =
    Either.cond(response.headers.exists(_.lowercaseName == "x-embedded-crowd-version"), response, NotCrowdService)

  def translateErrorEntity(implicit mat: Materializer, ec: ExecutionContext): Either[CrowdError, HttpResponse] ⇒ Future[Either[CrowdError, HttpResponse]] = {
    case Right(httpResponse) =>
      if (httpResponse.status.isFailure) {
        Unmarshal(httpResponse).to[ErrorEntity].
          map(_.translate).
          map(decodeResult => {
            val err = decodeResult.toRight(RawError(httpResponse.entity.toString.right.getOrElse("HTTP response deserialisation failed"), httpResponse.headers.map(h ⇒ h.name -> h.value).toMap))
            Left(CrowdHttpError(httpResponse.status.intValue, err)): Either[CrowdError, HttpResponse]
          }).recover({
            case _ =>
              val err = Left(RawError(httpResponse.entity.toString.right.getOrElse("HTTP response deserialisation failed"), httpResponse.headers.map(h ⇒ h.name -> h.value).toMap))
              Left(CrowdHttpError(httpResponse.status.intValue, err)): Either[CrowdError, HttpResponse]
          })
      } else
        Future.successful(Right(httpResponse): Either[CrowdError, HttpResponse])
    case res @ Left(_) => Future.successful(res: Either[CrowdError, HttpResponse])
  }
}

trait CrowdUserManagementResources {
  val crowdBaseUri: Uri

  val userManagementBaseUri: Uri = crowdBaseUri.withPath(crowdBaseUri.path / "rest" / "usermanagement" / "1")
}

trait SprayRestCrowdClientRequests extends CrowdUserManagementResources {
  import CrowdJsonProtocol._

  val userResourceUri: Uri = userManagementBaseUri.withPath(userManagementBaseUri.path / "user")
  val userAttributeResourceUri: Uri = userManagementBaseUri.withPath(userManagementBaseUri.path / "user" / "attribute")
  val userPasswordResourceUri: Uri = userManagementBaseUri.withPath(userManagementBaseUri.path / "user" / "password")
  val renameUserResourceUri: Uri = userManagementBaseUri.withPath(userManagementBaseUri.path / "user" / "rename")
  val userGroupDirectResourceUri: Uri = userManagementBaseUri.withPath(userManagementBaseUri.path / "user" / "group" / "direct")
  val groupResourceUri: Uri = userManagementBaseUri.withPath(userManagementBaseUri.path / "group")
  val searchResourceUri: Uri = userManagementBaseUri.withPath(userManagementBaseUri.path / "search")
  val authResourceUri: Uri = userManagementBaseUri.withPath(userManagementBaseUri.path / "authentication")
  val sessionResourceUri: Uri = userManagementBaseUri.withPath(userManagementBaseUri.path / "session")
  val configResourceUri: Uri = userManagementBaseUri.withPath(userManagementBaseUri.path / "config")

  def getUserByName(username: String): CrowdRequest[User] =
    crowdRequest[User](Get(userResourceUri.withQuery(Uri.Query("username" -> username))))

  def getUserWithAttributes(username: String): CrowdRequest[UserWithAttributes] =
    crowdRequest[UserWithAttributes](Get(userResourceUri.withQuery(Uri.Query("username" -> username, "expand" -> "attributes"))))

  def getUserByKey(key: String): CrowdRequest[User] =
    crowdRequest[User](Get(userResourceUri.withQuery(Uri.Query("key" -> key))))

  def createUser(user: UserTemplate)(implicit ec: ExecutionContext): CrowdRequest[Option[UserWithAttributes]] =
    crowdRequest[Option[UserWithAttributes]](Post(userResourceUri, user))

  def updateUser(user: User,
    newFirstName: Option[String] = None, newLastName: Option[String] = None, newDisplayName: Option[String] = None,
    newEmail: Option[String] = None, newActive: Option[Boolean] = None)(implicit ec: ExecutionContext): CrowdRequest[Unit] = {

    val updatedUser = user.copy(
      firstName = newFirstName.getOrElse(user.firstName),
      lastName = newLastName.getOrElse(user.lastName),
      displayName = newDisplayName.getOrElse(user.displayName),
      email = newEmail.getOrElse(user.email),
      active = newActive.getOrElse(user.active),
      key = None)
    crowdRequest[Unit](Put(userResourceUri.withQuery(Uri.Query("username" -> user.name)), updatedUser))
  }

  def setPassword(username: String, password: PlainTextCredential)(implicit ec: ExecutionContext): CrowdRequest[Unit] =
    crowdRequest[Unit](Put(userPasswordResourceUri.withQuery(Uri.Query("username" -> username)), password))

  def renameUser(username: String, newUsername: String)(implicit ec: ExecutionContext): CrowdRequest[User] =
    crowdRequest[User](Post(renameUserResourceUri.withQuery(Uri.Query("username" -> username)), Map("new-name" -> newUsername)))

  def mergeUserAttributes(username: String, attributes: Map[String, Set[String]])(implicit ec: ExecutionContext): CrowdRequest[Unit] =
    crowdRequest[Unit](Post(userAttributeResourceUri.withQuery(Uri.Query("username" -> username)), Attributes(attributes)))

  def removeUserAttributes(username: String, attributeName: String): CrowdRequest[Unit] =
    crowdRequest[Unit](
      Delete(userAttributeResourceUri.withQuery(Uri.Query("username" -> username, "attributename" -> attributeName))))

  def removeUser(username: String): CrowdRequest[Unit] =
    crowdRequest[Unit](
      Delete(userResourceUri.withQuery(Uri.Query("username" -> username))))

  def searchUsers(cql: String, startIndex: Int, maxResults: Int): CrowdRequest[List[User]] =
    crowdRequest[List[User]](
      Get(searchResourceUri.withQuery(Uri.Query(
        "expand" -> "user",
        "entity-type" -> "user",
        "start-index" -> startIndex.toString,
        "max-results" -> maxResults.toString,
        "restriction" -> cql))))

  def validatePassword(username: String, password: String)(implicit ec: ExecutionContext): CrowdRequest[Option[UserWithAttributes]] =
    crowdRequest[Option[UserWithAttributes]](Post(authResourceUri.withQuery(Uri.Query("username" -> username)), Map("value" -> password)))

  private def crowdRequest[A: FromResponseUnmarshaller](req: HttpRequest): CrowdRequest[A] =
    CrowdRequest(req, implicitly[FromResponseUnmarshaller[A]])

  implicit val emptyResponseBodyAsUnit: Unmarshaller[HttpEntity, Unit] = Unmarshaller(_ => _ => Future.successful(()))
}

class SprayRestCrowdClient(val crowdBaseUri: Uri,
  val appName: String, val appPassword: String,
  val timeout: Timeout = Timeout(10.seconds))(implicit val system: ActorSystem, implicit val mat: Materializer, implicit val executionContext: ExecutionContext)
    extends CrowdResponseTransformers
    with CrowdClient[Future] with SprayRestCrowdClientRequests {

  implicit val requestTimeout: Timeout = timeout

  def performRequest[A](request: CrowdRequest[A]): Future[Either[CrowdError, A]] =
    executeRequestAndObtainResult(request)

  private def executeRequestAndObtainResult[T](request: CrowdRequest[T]): Future[Either[CrowdError, T]] = {
    val httpRequest = request.request
    implicit val unmarshaller: FromResponseUnmarshaller[T] = request.unmarshaller

    val authorization = Authorization(BasicHttpCredentials(appName, appPassword))

    sendReceive(httpRequest.mapHeaders(_ :+ authorization :+ Accept(MediaTypes.`application/json`))).
      map(validateCrowdService).
      flatMap(translateErrorEntity).
      flatMap({
        case Left(err) => Future.successful(Left(err): Either[CrowdError, T])
        case Right(resp) => Unmarshal(resp).to[T].map(Right(_): Either[CrowdError, T])
      })

  }

  // extracted so it can be overridden in the tests
  protected def sendReceive: HttpRequest => Future[HttpResponse] = SprayRestCrowdClient.sendReceiveHttps
}

final case class CrowdRequest[A](request: HttpRequest, unmarshaller: FromResponseUnmarshaller[A])

object SprayRestCrowdClient {
  def sendReceiveHttps(implicit system: ActorSystem, mat: Materializer): HttpRequest => Future[HttpResponse] = { request =>
    Http().singleRequest(request)
  }
}
