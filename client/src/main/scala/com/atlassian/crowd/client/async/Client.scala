package com.atlassian.crowd.client.async

import com.atlassian.crowd.client.spray.CrowdRequest

import scala.concurrent.Future

trait CrowdClient[F[_]] {
  def performRequest[A](request: CrowdRequest[A]): Future[Either[CrowdError, A]]
}
